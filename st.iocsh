#!/usr/bin/env iocsh.bash
###############################################################################
#
# EPICS startup script to launch IOC using module to control an Andor SDK3 driver
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, Lund, 2020
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@ess.se
###############################################################################

# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require (adcore)
require (adandor3sim)
require (adpluginkafka)
require (busy)
require (calc)

# -----------------------------------------------------------------------------
# Environment variables
# -----------------------------------------------------------------------------
epicsEnvSet("PREFIX", "YMIR-ANDOR3-01")
epicsEnvSet("DEV",    "$(DEV=KFK)")
epicsEnvSet("PORT",   "$(PREFIX=YMIR-ANDOR3)")
epicsEnvSet("CAMERA", "0")
epicsEnvSet("QSIZE",  "21")
epicsEnvSet("XSIZE",  "2592")
epicsEnvSet("YSIZE",  "2160")
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "500")
epicsEnvSet("KFK_PORT",         "$(PREFIX)_KFK")
epicsEnvSet("KFK_SERVER_IP",    "$(KFK_SERVER_IP=dmsc-kafka01.cslab.esss.lu.se)")
epicsEnvSet("KFK_SERVER_PORT",  "$(KFK_SERVER_PORT=9092)")
epicsEnvSet("KFK_TOPIC",        "$(KFK_TOPIC=ymir_andor03_topic)")

# Configuring the connection with camera
# andor3Config(const char *portName, int cameraId, int maxBuffers,
#              size_t maxMemory, int priority, int stackSize,
#              int maxFrames)
andor3Config("$(PORT)", "$(CAMERA)", 0, 0, 0, 0, 10)

# Debugging 
# asynSetTraceMask("$(PORT)",0,255)
# asynSetTraceIOMask("$(PORT)",0,4)

#------------------------------------------------------------------------------
# Load ADAndor and ADBase databases
#------------------------------------------------------------------------------
dbLoadRecords("ADBase.template","P=$(PREFIX):,R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("NDFile.template","P=$(PREFIX):,R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("andor3.db",   "P=$(PREFIX):,R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")

#------------------------------------------------------------------------------
# Create a standard arrays plugin
#------------------------------------------------------------------------------
NDStdArraysConfigure("Image1", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("NDPluginBase.template","P=$(PREFIX):,R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")

#------------------------------------------------------------------------------
# Make NELEMENTS in the following be a little bigger than 2048*2048
#------------------------------------------------------------------------------
# 32-bit images
#------------------------------------------------------------------------------
# Use the following command for 32-bit images.  This is needed for 32-bit detectors or for 16-bit detectors in acccumulate mode if it would overflow 16 bits
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX):,R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=LONG,NELEMENTS=5600000")
#------------------------------------------------------------------------------
# 16-bit images
#------------------------------------------------------------------------------
# Use the following command for 16-bit images.  This can be used for 16-bit detector as long as accumulate mode would not result in 16-bit overflow
dbLoadRecords("NDStdArrays.template", "NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0,P=$(PREFIX):,R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=5600000")

# -----------------------------------------------------------------------------
# ADKafkaPlugin
# -----------------------------------------------------------------------------
KafkaPluginConfigure("$(KFK_PORT)", 3, 1, "$(PORT)", 0, -1, "$(KFK_SERVER_IP):$(KFK_SERVER_PORT)", "$(KFK_TOPIC)")
dbLoadRecords("ADPluginKafka.db", "P=$(PREFIX):, R=$(DEV):, PORT=$(KFK_PORT), ADDR=0, TIMEOUT=1, NDARRAY_PORT=$(PORT)")

#------------------------------------------------------------------------------
# Load all other plugins using commonPlugins.cmd
#------------------------------------------------------------------------------
< commonPlugins.cmd

# -----------------------------------------------------------------------------
# e3-common
# -----------------------------------------------------------------------------
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

#------------------------------------------------------------------------------
# Starting the IOC
#------------------------------------------------------------------------------
iocInit()

#------------------------------------------------------------------------------
# save things every thirty seconds
#------------------------------------------------------------------------------
#create_monitor_set("auto_settings.req", 30,"P=$(PREFIX):,D=cam1:")
#asynSetTraceMask($(PORT), 0, 255)

dbpf $(PREFIX):$(DEV):EnableCallbacks Enable
#dbpf $(PREFIX):cam1:AcquirePeriod 2
#dbpf $(PREFIX):cam1:Acquire 1
